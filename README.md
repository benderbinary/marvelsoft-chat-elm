# Marvelsoft chat elm
Project generated using https://github.com/soulehshaikh99/create-elm-electron-app
## To use
```bash
# Clone this repository
git clone https://gitlab.com/benderbinary/marvelsoft-chat-elm
# Install dependencies
npm install

# Run your app
npm run electron-dev

# Package Your App
npm run electron-pack
```