port module Main exposing (..)

import Browser
import Html exposing (Html, div, form, input, button, text)
import Html.Attributes exposing (class, id, placeholder, type_, value)
import Html.Events exposing (onSubmit, onInput)

type alias Model =
    { messages : List String
    , receivedMessage: List String
    , inputText : String
    }

init : Flags -> (Model, Cmd Msg)
init flags =
    ( { messages = []
      , receivedMessage = []
      , inputText = ""
      }
    , Cmd.none
    )

type Msg
    = InputChanged String
    | ReceiveMessage String
    | SendMessage

port receiveMessage : (String -> msg) -> Sub msg

port sendWebSocketMessage : String -> Cmd msg

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        InputChanged newText ->
            ( { model | inputText = newText }, Cmd.none )

        SendMessage ->
            let
                message = model.inputText
            in
            ( { model | receivedMessage = message :: model.receivedMessage, inputText = "" }, sendWebSocketMessage message )

        ReceiveMessage message ->
            ( { model | receivedMessage = message :: model.receivedMessage }, Cmd.none )

view : Model -> Html Msg
view model =
    div [ class "container" ]
        [ div [ class "messages" ]
            [  div [ class "received-messages-above" ]
                    (List.map viewMessage (List.reverse model.receivedMessage))
                , form [ id "messageForm", class "messageForm", onSubmit SendMessage ]
                    [ input
                        [ id "inputBox"
                        , placeholder "Type your message"
                        , onInput InputChanged
                        , value model.inputText
                        , class "input"
                        ]
                        []
                    , button
                        [ type_ "submit", class "btn" ]
                        [ text "Send" ]
                    ]
            ]
        ]

viewMessage : String -> Html Msg
viewMessage message =
    div [] [ text message ]

subscriptions : Model -> Sub Msg
subscriptions model =
    receiveMessage ReceiveMessage

type alias Flags =
    { }

main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
