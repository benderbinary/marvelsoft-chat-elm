import './main.css';
import { Elm } from './Main.elm';
import * as serviceWorker from './serviceWorker';

const app = Elm.Main.init({
  node: document.getElementById('marvelsoft-elm-container')
});

// Set up WebSocket connection
const socket = new WebSocket('ws://localhost:9898');

// Handle incoming WebSocket messages and send them to Elm
socket.onmessage = function (event) {
  const message = event.data;
  console.log('app.ports.receiveMessage  ', message)
  app.ports.receiveMessage.send(message);
};

// Handle outgoing messages from Elm and send them over WebSocket
app.ports.sendWebSocketMessage.subscribe(function (message) {
  console.log('app.ports.sendWebSocketMessage  ', message)
  socket.send(message);
});

serviceWorker.unregister();
